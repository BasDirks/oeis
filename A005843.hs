-- | The even numbers: a(n) = 2n.
module A005843 where


import Integer


f :: Z -> Z

f x = 2*x


a, b, c :: [Z]

a = [0,2..]

b = iterate (+2) 0

c = n & f
