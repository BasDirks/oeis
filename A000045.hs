-- | Fibonacci numbers: F(n) = F(n-1) + F(n-2) with F(0) = 0 and F(1) = 1.
module A000045 where

import Control.Monad
import Integer


-- $setup
-- >>> let xs = [0,1,1,2,3,5,8,13,21,34]


nacci :: Int -> [Z] -> [Z]
nacci x = liftM2 (++) init go
    where go xs = let p = sum (take x xs) in p : go (p : xs)

f, g :: Z -> Z

f = z 0 1
    where z _ y 0 = y
          z x y q = z y (x+y) (q-1)

g 0 = 0
g 1 = 1
g x = g (x-1) + g (x-2)


-- | >>> take 10 b == xs
-- True

-- | >>> take 10 a == xs
-- True

a, b :: [Z]

a = let z x y = x : z y (x+y) in z 0 1

b = nacci 2 [0,1]
