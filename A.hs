-- | a(n) = a(n-2) + A000265(a(n-1)), a(0)=0, a(1)=1.
module A114990 where


import qualified A000265
import Integer


a :: [Z]

a = let z x y = x : z y (x+A000265.f y) in z 0 1
