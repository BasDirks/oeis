-- | The squares: a(n) = n^2.
module A000290 where


import qualified A005408
import Integer
import Prelude hiding ((^))


f :: Z -> Z

f x = x^2


a, b :: [Z]

a = n & f

b = 0 : scanl1 (+) A005408.a
