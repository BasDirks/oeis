module Integer where


import Control.Applicative
import Prelude hiding ((/), (^))
import qualified Prelude as P



type Z = Integer


(^) :: Z -> Z -> Z
(^) = (P.^)


infixl 3 /


(/) :: Z -> Z -> Z
(/) = div


infixl 8 &

(&) :: Functor f => f a -> (a -> b) -> f b
x & f = f <$> x


n :: [Z]
n = [0..]


digits :: Z -> [Z]
digits = map (read . return) . show


iSqrt :: Integral a => a -> a -> a
iSqrt x a = if a == b then a else iSqrt x b
    where b = div x (div (a + div x a) 2)


iSqrt' :: Z -> Z
iSqrt' x = iSqrt x (x / 2)


isSqr :: Z -> Bool
isSqr x = iSqrt' x ^ 2 == x


infixr 1 !
(!) :: Z -> Z
(!) x = product [1..x]
