-- | Tribonacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) with a(0)=a(1)=0,
-- a(2)=1.
module A000073 where

import A000045 (nacci)
import Integer


f :: Z -> Z

f x = f (x-1) + f (x-2) + f (x-3)


a, b :: [Z]

a = let z w x y = x : z x y (w+x+y) in z 0 0 1

b = nacci 3 [0,0,1]
