-- | 2^n + 1.
module A000051 where


import Prelude hiding ((^))
import Integer


-- $setup
-- >>> let xs = [2,3,5,9,17,33,65,129,257,513]


f :: Z -> Z

f x = 2^x + 1


-- | >>> take 10 a == xs
-- True

a :: [Z]

a = n & f
