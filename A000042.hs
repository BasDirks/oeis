-- | Unary representation of natural numbers. 
module A000042 where


import Data.List
import Integer


-- $setup
-- >>> let xs = [1,11,111,1111,11111,111111,1111111,11111111,111111111,1111111111]


f :: Z -> Z

f x = read (genericReplicate x '1')


-- | >>> take 10 a == xs
-- True

a :: [Z]

a = tail n & f
