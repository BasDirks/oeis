-- | Number of labeled mappings from n points to themselves (endofunctions):
-- n^n.
module A000312 where


import Integer
import Prelude hiding ((^))


f :: Z -> Z

f x = x^x


a :: [Z]

a = n & f
