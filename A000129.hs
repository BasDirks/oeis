-- | Pell numbers: a(0) = 0, a(1) = 1; for n > 1, a(n) = 2*a(n-1) + a(n-2). 
module A000129 where


import Integer
import Prelude hiding ((/))


f :: Z -> Z

f 0 = 0
f 1 = 1
f x = 2*f (x-1) + f (x-2)


a :: [Z]

a = n & f
