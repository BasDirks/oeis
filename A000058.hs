-- | Sylvester's sequence: a(n+1) = a(n)^2 - a(n) + 1, with a(0) = 2.
module A000058 where


import Prelude hiding ((^))
import Integer


-- $setup
-- >>> let xs = [2,3,7,43,1807,3263443,10650056950807]


f :: Z -> Z

f 0 = 2
f x = f (x-1)^2 - f (x-1) + 1


-- | >>> take 7 a == xs
-- True

-- | >>> take 7 b == xs
-- True

a, b :: [Z]

a = iterate (\x -> x^2 - x + 1) 2

b = n & f
