-- | a(n) = a(n-2) + A000265(a(n-1)), a(0)=0, a(1)=1.
module A114990 where


import Integer
import Prelude hiding ((/))


a :: [Z]

a = let z x y = x : z y (x+f y) in z 0 1
        where f m = if even m then f (m/2) else m
