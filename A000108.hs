{-# LANGUAGE PostfixOperators #-}
-- | Catalan numbers: C(n) = binomial(2n,n)/(n+1) = (2n)!/(n!(n+1)!). Also 
-- called Segner numbers
module A000108 where


import Prelude hiding ((/))
import Integer


f :: Z -> Z

f x = (2*x!) / (x!)*(x+1!)


a :: [Z]

a = n & f
