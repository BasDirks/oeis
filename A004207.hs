-- | a(0) = 1, a(n) = sum of digits of all previous terms.
module A004207 where


import Control.Monad
import Integer


a :: [Z]

a = iterate (ap (+) (sum . digits)) 1
