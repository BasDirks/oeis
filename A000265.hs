-- | Remove 2's from n; or largest odd divisor of n; or odd part of n.
module A000265 where


import Integer
import Prelude hiding ((/))


f :: Z -> Z

f x = if even x then f (x/2) else x


a :: [Z]

a = n & f
