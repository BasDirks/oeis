-- | Factorial numbers: n! = 1*2*3*4*...*n (order of symmetric group S_n,
-- number of permutations of n letters)
module A000142 where


import Data.List
import Integer


f, g :: Z -> Z

f = (!)

g x = genericLength . permutations $ genericReplicate x ()


a, b, c :: [Z]

a = 1 : scanl1 (*) (tail n)

b = n & f

c = n & g
