-- | Pentagonal numbers: n*(3*n-1)/2.
module A000326 where


import Data.List
import Integer
import Prelude hiding ((/))


f :: Z -> Z
g :: Z -> Z

f x = x*(3*x-1) / 2

g x = sum . genericTake x $ [1,4..]


a, b :: [Z]

a = 0 : scanl1 (+) [1,4..]

b = n & f
