-- | Powers of 2: a(n) = 2^n. 
module A000079 where

import Data.List
import Integer
import Prelude hiding ((^))


f, g :: Z -> Z

f x = 2^x

g x = genericLength . subsequences $ genericReplicate x ()


a, b, c :: [Z]

a = n & f

b = iterate (*2) 1

c = n & g
