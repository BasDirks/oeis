-- | Jacobsthal sequence (or Jacobsthal numbers): a(n) = a(n-1) +
-- 2*a(n-2), with a(0) = 0, a(1) = 1.
module A001045 where


import Prelude hiding ((/), (^))
import Integer


f, g, h, i :: Z -> Z

f x = (2^x)-(-1)^x / 3

g 0 = 0
g 1 = 1
g x = f (x-1) + 2*f (x-2)

h 0 = 0
h x = 2*h (x-1) + (-1)^(x-1)
    
i = go . pred
    where go 0 = 0
          go 1 = 1
          go x = (2^x) - go (x-1)


a, b, c, d :: [Z]

a = n & f

b = n & g

c = n & h

d = 0 : 1 : n & i
