-- | Tetrahedral (or triangular pyramidal) numbers: a(n) = C(n+2,3) =
-- n*(n+1)*(n+2)/6
module A000292 where


import Integer
import Prelude hiding ((/))


f, g :: Z -> Z

f x = sum ([1..x] >>= enumFromTo 1)

g x = x*(x+1)*(x+2) / 6


a, b :: [Z]

a = n & f

b = n & g
