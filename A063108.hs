-- | a(1) = 1; a(n+1) = a(n) + product of nonzero digits of a(n).
module A063108 where


import Control.Monad
import Integer


a :: [Z]

a = iterate (ap (+) (product . digits)) 1
