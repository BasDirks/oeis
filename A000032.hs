-- | Lucas numbers (beginning at 2): L(n) = L(n-1) + L(n-2).
module A000032 where


import Integer


-- $setup
-- >>> let xs = [2,1,3,4,7,11,18,29,47,76]


-- | >>> take 10 a == xs
-- True

a :: [Z]

a = let z x y = x : z y (x+y) in z 2 1
