-- | Square pyramidal numbers: 0^2 + 1^2 + 2^2 +...+ n^2 = n*(n+1)*(2*n+1)/6.
-- TODO: test f vs g
module A000330 where


import qualified A000290
import Data.List
import Integer
import Prelude hiding ((/), (^))


f, g :: Z -> Z

f x = sum $ genericTake (x+1) A000290.a

g x = x*(x+1)*(2*x+1) / 6


a :: [Z]

a = scanl1 (+) A000290.a
