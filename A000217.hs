-- | Triangular numbers: a(n) = C(n+1,2) = n(n+1)/2 = 0+1+2+...+n.
module A000217 where


import Integer
import Prelude hiding ((/))


f, g :: Z -> Z

f x = x*(x+1) / 2

g x = sum [1..x]


a, b, c :: [Z]

a = scanl1 (+) n

b = n & f

c = n & g
