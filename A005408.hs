-- | The odd numbers: a(n) = 2n+1. 
module A005408 where


import Integer


f :: Z -> Z

f x = 2*x + 1


a, b, c :: [Z]

a = [1,3..]

b = iterate (+2) 1

c = n & f
