-- | Tetranacci numbers: a(n) = a(n-1) + a(n-2) + a(n-3) + a(n-4) with
-- a(0)=a(1)=a(2)=0, a(3)=1
module A000078 where

import Integer
import A000045 (nacci)


a :: [Z]

a = nacci 4 [0,0,0,1]
